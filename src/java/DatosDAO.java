
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author carlos
 */
public class DatosDAO {
        private Connection con;
    
    private void obtenerConexion() {        
        try {            
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "root");
        } catch (ClassNotFoundException | SQLException ex) {
            
        }
    }
    
    public List grafica(){
            try {
                CallableStatement cs = null;
                ResultSet rs = null;
                List lista = new ArrayList();
                obtenerConexion();
                cs = con.prepareCall("{call spDatos()}");
                rs = cs.executeQuery();
                while(rs.next()){
                    Datos datos = new Datos();
                    datos.setCantidad(rs.getInt("Alumnos"));
                    datos.setNombre(rs.getString("carrera"));
                    lista.add(datos);
                }
                
                return lista;
            } catch (SQLException ex) {
                
            }
            return null;
    }
    /*public static void main(String[] args) {
        DatosDAO dao = new DatosDAO();
        List lista = dao.grafica();
        for(int i = 0; i<lista.size();i++){
            Datos d = (Datos)lista.get(i);
            System.out.print(d.getNombre());
            System.out.println("  "+d.getCantidad());
        }
    }*/
    
}
