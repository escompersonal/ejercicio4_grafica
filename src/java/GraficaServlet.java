import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
//jFreeChart
/**
 *
 * @author carlos
 */
public class GraficaServlet extends HttpServlet {
    
    
    private DefaultPieDataset getGraficaAlumnos(){
        DefaultPieDataset pie = new DefaultPieDataset();
        DatosDAO dao = new DatosDAO();
        try{
            List datos = dao.grafica();
            for(int i = 0; i < datos.size();i++){
                Datos losDatos = (Datos) datos.get(i);
                pie.setValue(losDatos.getNombre(),losDatos.getCantidad());
                
            }
        }catch(Exception e){
            System.out.println("algo");
            
        }
        return pie;
    }
    
    private void generarGrafica(HttpServletRequest request, HttpServletResponse response) throws IOException{
        JFreeChart chart = ChartFactory.createPieChart("Alumnos por carrera",getGraficaAlumnos(), true, true,Locale.getDefault());
        
        String archivo=getServletConfig().getServletContext().getRealPath("/grafica1.png");
        System.out.println(archivo);
        ChartUtilities.saveChartAsPNG(new File(archivo), chart, 700, 400);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            generarGrafica(request, response);
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GraficaServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.print("<img src=\"grafica1.png\" >");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
